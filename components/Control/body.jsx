import React from 'react'
import { Image,Button } from 'antd';

export default function BodyMelon() {
    return(
        <div className='flex h-screen  bg-cover bg-center relative '
        // style={{
        //     backgroundImage: `url(https://firebasestorage.googleapis.com/v0/b/image-gearlap-upload-596f7.appspot.com/o/image%2Fmelon-whole-seamless-pattern-on-1549736.jpg?alt=media&token=41a11368-a2f0-4f75-a349-752e15d9ccff)`,
        // }}
    >
        <div className="flex flex-col items-center space-y-16 bg-orange-500 rounded-lg hover:shadow-xl m-8 w-1/3 h-1/2 p-10">
            <div className='flex space-x-3'>
            <Image
      width={50}
      src="https://mechatronics.by/wp-content/uploads/2014/04/temp-icon-300x300.png"
    />
              

            <p className='text-3xl font-medium text-white'>Nhiệt độ</p>
            </div>
            
            <p className='text-6xl font-medium text-white'>35 C</p>
            </div>
        <div className="flex flex-col items-center space-y-16 bg-blue-500 rounded-lg hover:shadow-xl m-8 w-1/3 h-1/2 p-10">
            <div className='flex space-x-3'>
            <Image
      width={50}
      src="https://cdn-icons-png.flaticon.com/512/219/219816.png"
    />
              

            <p className='text-3xl font-medium text-white'>Độ ẩm</p>
            </div>
            
            <p className='text-6xl font-medium text-white'>80%</p>
            </div>
            <div className="flex flex-col items-center space-y-3 bg-white rounded-lg hover:shadow-xl m-8 w-1/3 h-1/2 p-10">
            <Image
      width={250}
      src="https://icons.iconarchive.com/icons/vexels/office/512/bulb-icon.png"
    />
     <div className='flex space-x-3'>
            <Button style={{background:"green", color:'white'}}>ON</Button>
            <Button style={{background:"red", color:'white'}}>OFF</Button>
            </div>
            </div>
           
            
        </div>
       
    )
}
