import React from 'react'
import { Image,Button } from 'antd';

export default function Banner() {
    return(
        <div
        className="bg-no-repeat bg-cover bg-center cursor-pointer"
        style={{
            width: 'auto',
            height: 700,
            backgroundImage: `url(https://i.pinimg.com/564x/8a/d6/67/8ad6679b3fa03a5bd9069d8afd959a47.jpg)`,
        }}
    >
         <div 
                        className="absolute opacity-70  top-0 w-full h-full z-30  group-hover:bg-gray-500 flex justify-center items-center">
                       <p className='text-5xl font-medium text-black'>Hi-tech Melon Garden</p>
                    </div>

    </div>

    )
}