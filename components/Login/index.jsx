import React, { useEffect, useState } from 'react';
import Router from 'next/router'
import { message, Select } from 'antd';
import classNames from "classnames";
import Link from 'next/link'

import LoginNormal from './loginNormal';
import SignUp from './signUp';



export default function Login() {
    const [tab, setTab] = useState("dang-nhap");
    const [hover, setHover] = useState(false);


    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    return(
        <div className='flex h-screen justify-center bg-cover bg-center relative'
        style={{
            backgroundImage: `url(https://firebasestorage.googleapis.com/v0/b/image-gearlap-upload-596f7.appspot.com/o/image%2Fmelon-whole-seamless-pattern-on-1549736.jpg?alt=media&token=41a11368-a2f0-4f75-a349-752e15d9ccff)`,
        }}
    > <div style={{ top: tab === 'dang-nhap' ? '30%' : '45%' }} className="absolute left-1/2 transform -translate-x-1/2 -translate-y-1/2 my-6 p-10 bg-white rounded-xl hover:shadow-xl z-0 w-96">
              <div className='text-center text-lg mb-4'><Link href="/">
                <a className="text-green-500 text-3xl text-center" >Melon</a>
              </Link></div>
         
         <div className="text-center  text-xl font-normal" style={{ fontFamily: "muli,sans-serif" }}>
                    <div className="inline-block mr-10"
                    >
                        <p
                            className={classNames("cursor-pointer", {
                                "text-2xl text-blue-500": tab === "dang-nhap",
                            }, {
                                "text-xl": hover === "dang-ki",
                            })}
                            onClick={() => setTab("dang-nhap")}
                        >
                            Đăng nhập
                        </p>
                    </div>
                    <div className="inline-block "
                    >
                        <p
                            className={classNames("cursor-pointer", {
                                "text-2xl text-green-500": tab === "dang-ki",
                            })}
                            onClick={() => setTab("dang-ki")}
                        >
                            Đăng kí
                        </p>
                    </div>
                </div>
                {tab === "dang-nhap" &&
                    <LoginNormal onFinishFailed={onFinishFailed} />
                }
                 {tab === "dang-ki" &&
                    <SignUp  onFinishFailed={onFinishFailed}  />
                }


        </div>

</div>
    )
}