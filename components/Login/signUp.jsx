import React from 'react';
import { DatePicker, Form, Input, Button, Select, Steps,message } from 'antd';

const { Step } = Steps;


export default function  SignUp({onFinishFailed})  {



    return(
        <div >
     
             <Form
                                layout="vertical"
                                name="basic"
                                // onFinish={handleRegister}
                                onFinishFailed={onFinishFailed}
                                autoComplete="off"
                            >
                                {/* {tabSignUp==="tai-khoan" && <> */}
                                <Form.Item
                                   
                                    name="email"
                                    rules={[
                                        {
                                            required: true,
                                            type: 'email',
                                            message: 'Nhập đúng email!!!',
                                        },
                                    ]}
                                >
                                    <Input  placeholder="Email"/>
                                </Form.Item>

                                <Form.Item
                                    name="password"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Mật khẩu không được để trống và độ dài trên 6 kí tự!',
                                            min: 6,
                                        },
                                    ]}
                                >
                                    <Input.Password placeholder="Mật khẩu"/>
                                </Form.Item>
                                {/* </>
                                }
                                {tabSignUp==="thong-tin" && <> */}
                                <Form.Item
                                    name="name"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Tên đầy đủ không được để trống!',
                                        },
                                    ]}
                                >
                                    <Input placeholder="Họ và tên"/>
                                </Form.Item>
                                <Form.Item
                                    name="birthdate"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Ngày sinh không được để trống!',
                                        },
                                    ]}
                                >
                                    <DatePicker placeholder="Ngày sinh" style={{ width: "100%" }}/>
                                </Form.Item>
                              
                                <Form.Item
                                    name="phone"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Số điện thoại có độ dài là 10 số!',
                                            len: 10,
                                        },
                                    ]}
                                >
                                    <Input type="number" placeholder="Số điện thoại"/>
                                </Form.Item>

                                <Form.Item
                                    className="text-center"
                                >
                                    <Button type="primary" htmlType="submit" style={{ width: "100%" }}>
                                        Đăng kí</Button>
                                </Form.Item>
                                {/* </>
                                } */}
                            </Form>

        </div>
    )

}
