import React, { useEffect, useState } from "react";
import Link from 'next/link'
import { Layout, Menu, Input, Button, Breadcrumb, Badge, BackTop, Carousel, Dropdown, message, Tooltip } from "antd";
import { UserOutlined,  MailOutlined, AppstoreOutlined, SettingOutlined } from "@ant-design/icons";
import Router from 'next/router';
const { Header, Footer, Content, Sider } = Layout;
const { SubMenu } = Menu;
const { Search } = Input;
export default function DefaultLayout({ children }) {

    return (
     
      
        <Layout className="rounded-xl p-8 md:p-0 ">
        <Header  style={{ background: "black" }} className="sticky z-50 top-0">
            <div className="flex justify-between items-center space-x-10">
            <Link href="/">
                <a className="text-2xl text-green-500" >Melon</a>
              </Link>
              {/* <div className="flex space-x-4 items-center w-full">

              <Link href="/">
                <a className="text-2xl text-green-500" >Melon</a>
              </Link> */}
              <div className="w-full">
              <Menu 
              style={{ background: "black" , width:"100%"}}
            className="Menu"
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={["1"]}
          >
            <Menu.Item key="1">
              Home
            </Menu.Item>
            <Menu.Item key="2">
              About
            </Menu.Item>
            <Menu.Item key="3">
              Member
            </Menu.Item>
            <Menu.Item key="4">
              About
            </Menu.Item>
          
          </Menu>
              </div>
           

              {/* </div> */}
                  
              
            <Tooltip placement="bottom" title="Đăng nhập">
                <UserOutlined onClick={() => Router.push('/login')} style={{ fontSize: 24, color: "white" }} />
              </Tooltip>
              </div>
         
        </Header>

          <Content className="site-layout">

  <div className="bg-gray-100 ">
    <div style={{ minHeight: 650 }} className=" mx-auto">
      
     {children}
      </div>

      </div>
    </Content>

          <Footer>
          <div className=" text-center">
          ©Trang web của project Movies
        </div>

          </Footer>
        </Layout>
 
    )
}