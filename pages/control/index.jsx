import Head from 'next/head'
import Control from '../../components/Control'
import Login from '../../components/Login'

export default function HomePage() {
    return (
        <>
            <Head>
                <title>Trang điều khiển</title>
            </Head>

         <Control/>
        </>
    )
}